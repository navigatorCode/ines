.include "nes.inc"
.segment           "CHARS"
.incbin        "ascii.chr"

.segment            "STARTUP"

start:
    jsr test_log
    sei
    clc
    lda #$10
    jsr test_lda
    pha
    clc
    adc #$01
    jsr test_adc
    pla
    jsr test_pla_pha
    sta $00
    lda $00
    jsr test_sta

irq:


nmi:

    .byte LOG,"CPU test passed.",NULL
forever:
    jmp forever

test_log:
    .byte LOG,"Start exec cpu test....",NULL
    rts

test_lda:
    cmp #$10
    beq test_lda_pass
    .byte LOG,"lda is execute fail.",NULL
    jmp forever
test_lda_pass:
    rts

test_adc:
    cmp #$11
    beq test_adc_pass
    .byte LOG,"adc is execute fail.",NULL
    jmp forever
test_adc_pass:
    rts

test_pla_pha:
    cmp #$10
    beq test_pla_pha_pass
    .byte LOG,"pla or pha is execute fail.",NULL
    jmp forever
test_pla_pha_pass:
    rts

test_sta:
    cmp #$10
    beq test_sta_pass
    .byte LOG,"sta(Zero_Page) execute fail.",NULL
    jmp forever
test_sta_pass:
    rts