;
; Apache License, Version 2.0
;
; Copyright (c) 2023 杨奎 (Kui Yang)
;
;
; Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
;
;   http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
;
.include "nes.inc"

.segment           "CHARS"
.incbin        "ascii.chr"

.segment            "STARTUP"

start:
    sei
    clc
    lda #$80
    sta PPU_CTRL                    ;Enable val flag
    jmp waitvbl

irq:
    jmp waitvbl

nmi:
    lda #$00
    sta PPU_MASK
    lda PPU_STATUS                  ; Reset w to 0
    lda #$20
    sta PPU_ADDR
    lda #$40
    sta PPU_ADDR                    ; Set VRAM address to $2000
    ldx #$00
TEXT_RENDER:                      ; Write vendor name
    lda VENDOR_TEXT,x
    sta PPU_DATA
    INX
    CPX #$0B
    BNE TEXT_RENDER
    LDA #$00
    STA PPU_SCROLL
    LDA PPU_STATUS                  ; Reset w to 0
    LDA #$3f                        ; Set VRAM address to $3f01
    STA PPU_ADDR
    LDA #$01
    STA PPU_ADDR
palette:
    STA PPU_DATA
    ADC #$01
    CMP #$0f
    BNE palette
    LDA #$00                              ; Set x scroll was 0
    STA PPU_SCROLL
    lda #$00
    STA PPU_SCROLL
    LDA #$80
    STA PPU_CTRL                    ;Switch to first name table
    lda #%00001010                  ;Enable background show
    sta PPU_MASK
    jmp forever


forever:
    jmp forever

waitvbl:
    jmp waitvbl

VENDOR_TEXT:
; hello,world
.byte $28,$25,$2C,$2C,$2F,$0C,$37,$2F,$32,$2C,$24