.include "nes.inc"

.segment "CHARS"
.incbin "ascii.chr"

.segment "STARTUP"

start:
    sei
    clc
    lda #$01
    asl a     ; a = 2
    tax
    cpx #$02
    lsr a     ; a = 1
    tax
    cpx #$01
    dex  ; x = 0
    dex  ; x = $ff
    jsr oop
    lsr

nmi:
    jmp nmi

irq:
    jmp irq

oop:
    nop
    nop
    nop
    nop
    rts