#include "../hs/mapper.h"

static String CARTRIDGE_NAME = "NROM";

static String ines_mapper_name() {
    return CARTRIDGE_NAME;
}

static uint8 ines_chr_read(NesConsole *console, uint16 addr) {
    return console->chrRom[addr];
}

static uint8 ines_prg_read(NesConsole *console, uint16 addr) {
    uint32 prgSize = console->prgSize;
    if (prgSize == 16 * 1024 && addr > 0xC000) {
        addr = addr - 0xC000;
    } else {
        addr = addr - 0x8000;
    }
    return console->prgRom[addr];
}

static void ines_chr_write(NesConsole *console, uint16 addr, uint8 b) {

}

static void ines_prg_write(NesConsole *console, uint16 addr, uint8 b) {

}


extern void ines_NROM_register(NesConsole *console) {
    INES_CARTRIDGE_REGISTER_TEMPLATE(console->cartridge)
}