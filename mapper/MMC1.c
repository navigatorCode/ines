#include <malloc.h>
#include "../hs/mapper.h"

typedef struct {
    uint8 chrBank;
    uint8 prgBank;
} MMC1Private;

static String CARTRIDGE_NAME = "MMC1";

static String ines_mapper_name() {
    return CARTRIDGE_NAME;
}

static uint8 ines_chr_read(NesConsole *console, uint16 addr) {
    return 0;
}

static uint8 ines_prg_read(NesConsole *console, uint16 addr) {
    return 0;
}

static void ines_chr_write(NesConsole *console, uint16 addr, uint8 b) {

}

static void ines_prg_write(NesConsole *console, uint16 addr, uint8 b) {

}


extern void ines_MMC1_register(NesConsole *console) {
    CartridgeMapper *cartridge = console->cartridge;
    INES_CARTRIDGE_REGISTER_TEMPLATE(cartridge)

    MMC1Private *instance = malloc(sizeof(MMC1));

    instance->chrBank = 0;
    instance->prgBank = 0;

    cartridge->instance = instance;
}