#ifndef INES_PPU_H
#define INES_PPU_H

#include "ines.h"

extern PPU *ines_ppu_new();

extern void ines_ppu_dispose(PPU *ppu);

extern uint8 ines_ppu_status(NesConsole *console);

extern void ines_ppu_mask(NesConsole *console, uint8 b);

extern void ines_ppu_scroll(NesConsole *console, uint8 b);

extern void ines_ppu_addr(NesConsole *console, uint8 b);

extern void ines_ppu_control(NesConsole *console, uint8 b);

extern void ines_ppu_dma(NesConsole *console, uint8 b);

extern void ines_ppu_run(NesConsole *console, int masterCycle);

extern uint8 ines_ppu_read(NesConsole *console);

extern void ines_ppu_write(NesConsole *console, uint8 b);

extern uint8 ines_ppu_oam_read(NesConsole *console);

extern void ines_ppu_oam_write(NesConsole *console, uint8 b);

#endif //INES_PPU_H
