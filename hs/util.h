#ifndef INES_UTIL_H
#define INES_UTIL_H

#include "ines.h"
#include <stdio.h>

/**
 * Alloc byte array and fill 0
 */
extern uint8 *ines_new_array(uint32 size);

/**
 * Calculator file size
 */
extern uint64 ines_file_size(FILE *file);

/**
 * Read all byte from target path
 * @param path File path
 * @param length File length pointer
 * @return uint8 byte array
 */
extern uint8 *ines_fread_all(String path, uint64 *length);

#endif //INES_UTIL_H
