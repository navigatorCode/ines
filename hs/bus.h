#ifndef INES_BUS_H
#define INES_BUS_H

#include "ines.h"


extern MemBus *ines_bus_new();

extern void ines_bus_dispose(MemBus *bus);

extern void ines_bus_sram_fill(MemBus *bus, const uint8 *buf, int offset, int size);

extern uint8 ines_bus_read(NesConsole *console, uint16 addr);

/**
 *
 * Read utf8 char end of '\0'
 *
 */
extern int ines_bus_read_utf8char(NesConsole *console, uint8 *arr, int length);

extern void ines_bus_write(NesConsole *console, uint16 addr, uint8 b);

extern void ines_bus_stack_push(NesConsole *console, uint8 b);

extern void ines_bus_stack_push0(NesConsole *console, uint16 b);

extern uint8 ines_bus_stack_pop(NesConsole *console);

extern uint16 ines_bus_stack_pop0(NesConsole *console);

extern uint16 ines_bus_read_uint16(NesConsole *console, uint16 addr);

#endif //INES_BUS_H
