#ifndef INES_CPU_H
#define INES_CPU_H

#include "ines.h"


extern CPU *ines_cpu_new();

extern void ines_cpu_dispose(CPU *cpu);

/**
 * External irq
 */
extern void ines_console_irq(CPU *cpu, ExternalIRQ external);

#endif //INES_CPU_H
