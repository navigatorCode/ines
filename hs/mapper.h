#ifndef INES_MAPPER_H
#define INES_MAPPER_H

#include "type.h"

#define INES_CARTRIDGE_REGISTER_TEMPLATE(cartridge) \
{                                                   \
        cartridge->ines_chr_read = ines_chr_read; \
        cartridge->ines_prg_read = ines_prg_read; \
        cartridge->ines_chr_write = ines_chr_write; \
        cartridge->ines_prg_write = ines_prg_write;   \
        cartridge->ines_mapper_name = ines_mapper_name; \
}

extern void ines_NROM_register(NesConsole *console);

extern void ines_MMC1_register(NesConsole *console);

#endif //INES_MAPPER_H
