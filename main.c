#include "util.h"
#include <errno.h>
#include <string.h>

static uint8 pixels[PPU_VIDEO_HEIGHT * PPU_VIDEO_WIDTH * 3];

static void ines_game_callback(const uint8 *pixelBuf) {
    for (int i = 0; i < PPU_VIDEO_HEIGHT; ++i) {
        uint32 offset = i * PPU_VIDEO_WIDTH;
        for (int j = 0; j < PPU_VIDEO_WIDTH; ++j) {
            uint32 k = (offset + j) * 3;
            uint8 idx = pixelBuf[k] & 0x3F;
            const uint8 *pixel = DEF_PPU_PALETTE[idx];
            pixels[k] = pixel[0];
            pixels[k + 1] = pixel[1];
            pixels[k + 2] = pixel[3];
        }
    }
}

int main() {
    uint8 *buffer = ines_fread_all("../rom/ines.nes", NULL);
    NesConsole *console = ines_console_new(buffer, ines_game_callback);
    if (console == NULL) {
        String str = ines_error_text(errno);
        printf("Console instance fail:%s\n", str);
        return -1;
    }
    while (True) {
        ines_console_execute(console);
    }

    ines_console_dispose(&console);
    return 0;
}
