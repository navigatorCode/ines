#include <stdio.h>
#include <malloc.h>
#include <memory.h>
#include "util.h"

extern uint8 *ines_new_array(uint32 size) {
    if (size == 0) {
        return NULL;
    }
    uint8 *array = malloc(size);
    memset(array, 0, size);
    return array;
}

extern uint64 ines_file_size(FILE *file) {
    int64 tmp = ftell(file);
    fseek(file, 0, SEEK_END);
    uint64 size = ftell(file);
    fseek(file, tmp, SEEK_SET);
    return size;
}

extern uint8 *ines_fread_all(String path, uint64 *length) {
    FILE *file = fopen(path, "r");
    if (file == NULL) {
        return NULL;
    }
    uint64 size = ines_file_size(file);
    uint8 *buffer = malloc(sizeof(uint8) * size);
    fread(buffer, 1, size, file);
    if (length != NULL) {
        *length = size;
    }
    fclose(file);
    return buffer;
}
